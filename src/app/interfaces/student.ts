export interface Student {
    
        avm: number;
        psi: number;
        complete: boolean;
        id?:string;
        saved?:Boolean;
        result?:string;
    }

