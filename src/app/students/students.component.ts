import { StudentService } from './../students.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId;

  students:Student[];
  students$;
  addStudentFormOpen;
  rowToEdit:number = -1; 
  studentToEdit:Student = {avm:null, psi:null, complete:null};

  add(student:Student){
    this.studentsService.addStudent(this.userId, student.avm, student.psi, student.complete)
  }
  
  moveToEditState(index){
    console.log(this.students[index].avm);
    this.studentToEdit.avm = this.students[index].avm;
    this.studentToEdit.psi = this.students[index].psi;
    this.studentToEdit.complete = this.students[index].complete;
    this.rowToEdit = index; 
  }

  updateStudent(){
    let id = this.students[this.rowToEdit].id;
    this.studentsService.updateStudent(this.userId,id, this.studentToEdit.avm,this.studentToEdit.psi,this.studentToEdit.complete);
    this.rowToEdit = null;
  }

  deleteStudent(index){
    let id = this.students[index].id;
    this.studentsService.deleteStudent(this.userId, id);
  }

  updateResult(index){
    this.students[index].saved = true; 
    this.studentsService.updateRsult(this.userId,this.students[index].id,this.students[index].result);
  }

  predict(index){
    this.students[index].result = 'Will difault';
    this.predictionService.predict(this.students[index].avm, this.students[index].psi,this.students[index].complete).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will default'
        }
        this.students[index].result = result}
    );  
  }

  displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit', 'Predict', 'Result'];
 
  constructor(private studentsService:StudentService,
    private authService:AuthService,
    private predictionService:PredictionService ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.students$ = this.studentsService.getStudents(this.userId);
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const student:Student = document.payload.doc.data();
                if(student.result){
                  student.saved = true; 
                }
                student.id = document.payload.doc.id;
                   this.students.push(student); 
              }                        
            }
          )
      })
  }   
}