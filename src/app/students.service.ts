import { Student } from './interfaces/student';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class StudentService {

userCollection:AngularFirestoreCollection = this.db.collection('users');
customersCollection:AngularFirestoreCollection;

getStudents(userId): Observable<any[]> {
  this.customersCollection = this.db.collection(`users/${userId}/students`, 
     ref => ref.limit(10))
  return this.customersCollection.snapshotChanges();    
} 

updateRsult(userId:string, id:string,result:string){
  this.db.doc(`users/${userId}/students/${id}`).update(
    {
      result:result
    })
  }

updateStudent(userId:string, id:string,avm:number,psi:number,complete:boolean){
  this.db.doc(`users/${userId}/students/${id}`).update(
    {
      avm:avm,
      psi:psi,
     complete:complete,
      result:null
    }
  )
}

deleteStudent(userId:string, id:string){
  this.db.doc(`users/${userId}/students/${id}`).delete();
}

addStudent(userId:string, avm:number,psi:number,complete:boolean){
  const student:Student = {avm:avm, psi:psi, complete:complete}
  this.userCollection.doc(userId).collection('students').add(student);
} 



constructor(private db: AngularFirestore,
  ) {} 

}