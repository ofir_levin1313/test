import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = ' https://9ejazel17i.execute-api.us-east-1.amazonaws.com/beta'; 
  
  predict(avm:number, psi:number,
    complete:boolean){
    let json = {
      "data": 
        {
          "avm":avm,
          "psi": psi,
          "complete":complete
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  
  constructor(private http: HttpClient) { }
}